﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerMover : MonoBehaviour
{
	public static float totalScore = 0.0f;
	public static float speed = 1.0f;
	public static bool crashed = false;

	public bool forTouchDevice = false;
	//determines which controlls to use, touch or keyboard

	public float sidewaysSpeed = 5.0f;
	//car's sideways speed
	public float minX = -3.6f;
	//minimum X axis value the car can go
	public float maxX = 3.6f;
	//maximum X axis value the car can go
	public float tilt = 2.0f;
	//car tilt amount when moving sideways
	public float titltSpeed = 10.0f;
	//how fast the tilt happen

	public float coinScore = 5.0f;
	//determines how much score does coin give
	public float timeScoreSpeed = 5.0f;
	//determines how much score do you get in second
	public float turboDoublingScore = 2.0f;
	//determines how many times does score multiply

	public GameObject turboFire;
	//object which will be activated when using turbo

	public AudioClip crashSound;
	//the sound which will be played when car crashes
	public AudioClip coinSound;
	//the sound which will be played when picking up coin

	public Text restartText, quitText;
	//Text which will be activated when car crashes
	public Text gameOverText;
	//Text which will be activated when car crashes
	public Text scoreText;
	//Text which is used to show current score
	
	private float yPos = 0.0f;
	private Rigidbody2D rb2D;

	private float timeScore = 0.0f;
	private float coinScr = 0.0f;
	private bool doublingScore = false;
	private bool speedDoubled = false;
	private AudioSource audio;
	private float _delta;
	public bool _run;

	void Start ()
	{
		Application.targetFrameRate = 60;
		//reset values when game starts
		totalScore = 0;
		crashed = false;
		rb2D = GetComponent<Rigidbody2D> ();
		audio = GetComponent<AudioSource> ();
		yPos = rb2D.position.y;
		speed = 1.0f;
		_delta = 0.0f;
		_run = false;
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		//when car collides with other object which tag is 'Car' that means car is crashed
		//so we stop the car, making restart text and game over text visible and playing crash sound
		if (!crashed && other.tag == "Car") {
			rb2D.isKinematic = true;
			crashed = true;
			turboFire.SetActive (false);
			AdsControl.Instance.showAds ();
			//restartText.text = forTouchDevice ? "Tap To Restart" : "Press 'R' To Restart";
			restartText.gameObject.SetActive (true);
			quitText.gameObject.SetActive (true);
			gameOverText.enabled = true;
			audio.clip = crashSound;
			audio.loop = false;
			audio.Play ();
		}

		//if car collides to 'Coin' destroy it, play sound and add score
		if (other.tag == "Coin") {
			Destroy (other.gameObject);
			audio.PlayOneShot (coinSound);
			coinScr += coinScore;
		}

	}


	void FixedUpdate ()
	{
		//if car isn't crashed
		if (!crashed) {
			//move car depending on keyboard or accelerometer on touch device
			//float moveHorizontal = forTouchDevice ? Input.acceleration.x : Input.GetAxis ("Horizontal");

			float moveHorizontal = _delta;

			//move car sideways
			Vector3 movement = new Vector3 (moveHorizontal, 0.0f, 0.0f);
			rb2D.velocity = movement * sidewaysSpeed;

			//clamp X axis value, so car won't go more than 'minX' and 'maxX' values
			rb2D.position = new Vector3 (Mathf.Clamp (rb2D.position.x, minX, maxX), yPos, 0.0f);

			var velX = 0.0f;

			if (rb2D.position.x < maxX && rb2D.position.x > minX)
				velX = rb2D.velocity.x;

			//tilt the car depending on movement speed and tilt amount
			rb2D.rotation = Mathf.Lerp (rb2D.rotation, velX * -tilt, Time.deltaTime * titltSpeed);
		}
	}

	public void StartTurn (float _value)
	{
		_delta = _value;
	}

	public void EndTurn ()
	{
		_delta = 0;
	}

	public void StartRun ()
	{
		_run = true;
	}

	public void EndRun ()
	{
		_run = false;
	}


	void Update ()
	{
		//if car is crashed and R key is pressed or on touch device touch is detected, restart level
		if (crashed) {
			if ((Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) || Input.GetKey (KeyCode.R))
				Application.LoadLevel (Application.loadedLevel);
		} else {
			//if car isn't crashed and 'Vertical' keys are pressed or on touch device touch is detected, turn turbo
			//acttivate 'ruboFire' object, double score and speed, increase audio pitch
			if (_run) {
				if (!speedDoubled) {
					turboFire.SetActive (true);
					doublingScore = true;
					speed *= 2f;
					audio.pitch *= 1.1f;
					speedDoubled = true;
				}
			} else {
				//return back values which were changed during turbo
				if (speedDoubled) {
					turboFire.SetActive (false);
					doublingScore = false;
					speed /= 2f;
					audio.pitch /= 1.1f;
					speedDoubled = false;
				}
			}

			//add score for time
			if (doublingScore)
				timeScore += Time.deltaTime * timeScoreSpeed * turboDoublingScore;
			else
				timeScore += Time.deltaTime * timeScoreSpeed;

			//set totalscore and display it via 'scoreText'
			totalScore = coinScr + timeScore;
			scoreText.text = "Score: " + Mathf.Round (totalScore);
		}
	}

	public void Quit ()
	{

		UnityEngine.SceneManagement.SceneManager.LoadScene (0);
	}

	public void Restart ()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (1);
	}
}
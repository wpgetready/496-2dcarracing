﻿using UnityEngine;
using System.Collections;

public class RoadMover : MonoBehaviour {
	public float moveSpeed = -3.0f;		//speed of road
	public float endPositionY = -20.0f;	//Y axis value after the road will jump up
	public Transform secondRoad;		//when this road jumps up, used to position it corectly to second road
	public float offset = -0.1f;		//distance between roads

	private Transform tr;				//used to save this object's Transform and not use GetComponent method in update, good for performnace
	private Renderer secondRoadRend;	//used to save this object's Renderer and not use GetComponent method in update, good for performnace
	private Vector3 startPosition;		//used to save this object's start position
	private float yPos;					//used for changing road's Y axis value

	void Start()
	{
		tr = GetComponent<Transform>();	//save Transform component in 'tr' variable
		secondRoadRend = secondRoad.GetComponent<Renderer>();	//save Renderer component
		startPosition = tr.position;	//save start position
		yPos = startPosition.y;			//save start Y axis value
	}

	void Update()
	{
		//if player car isn't crashed move road depending on player car's speed
		if(!PlayerMover.crashed)
		{
			yPos += Time.deltaTime * moveSpeed * PlayerMover.speed;
			tr.position = new Vector3(startPosition.x, yPos, startPosition.z);
		}
	} 

	void LateUpdate()
	{
		//if player car isn't crashed and road's Y axis value is under 'endPositionY' jump up the road (align to second road)
		if(!PlayerMover.crashed)
		{
			if(tr.position.y < endPositionY)
			{
				tr.position = new Vector3
					(startPosition.x,
					 secondRoadRend.bounds.center.y + secondRoadRend.bounds.size.y + offset,
					 startPosition.z);

				yPos = tr.position.y;
			}
		}
	}
}
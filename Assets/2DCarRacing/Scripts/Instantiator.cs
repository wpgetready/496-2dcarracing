﻿using UnityEngine;
using System.Collections;

public class Instantiator : MonoBehaviour
{
	public GameObject[] objectsToInstantiate;	//objects which will be instantiated randomly
	public float startWait = 2.0f;				//wait given seconds before instantiation will be started
	public float spawnWait = 3.0f;				//wait time between each object instantiation
	public float waveWait = 4.0f;				//wait time after wave is over
	public float countPerWave = 5.0f;			//objects count to be instantiated for each wave
	

	void Start ()
	{
		StartCoroutine (SpawnWaves());	//start instantiation
	}


	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);	//wait 'startWait' seconds

		//if player car isn't crashed
		while (!PlayerMover.crashed)
		{
			//instantiate object 'countPerWave' times
			for (int i = 0; i < countPerWave; i++)
			{
				if(PlayerMover.crashed)
					break;

				//instantiate object randomly from 'objectsToInstantiate' array
				var obj = objectsToInstantiate[Random.Range (0, objectsToInstantiate.Length)];
				Instantiate (obj, transform.position, Quaternion.identity);

				//wait between each object instantiation
				yield return new WaitForSeconds (spawnWait / PlayerMover.speed);
			}
			//wait after wave is over
			yield return new WaitForSeconds (waveWait);
		}
	}
}
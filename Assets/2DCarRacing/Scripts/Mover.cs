﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour 
{
	public float speed = -3.0f;
	public bool checkDistance = false;
	public float distanceBetweenOtherObject = 3.0f;

	private Rigidbody2D rb2D;
	private RaycastHit2D hit;

	void Start ()
	{
		//save this object's Rigidbody2D component in 'rb2D' variable used in FixedUpdate, 
		//good for performance, GetComponent method is heavy, so we use it once.
		rb2D = GetComponent<Rigidbody2D>();	
	}

	void FixedUpdate ()
	{
		//if player car isn't crashed, change this object's velocity depending on player car's speed
		if(!PlayerMover.crashed)
		{
			//move object
			rb2D.velocity = new Vector2 (0.0f, speed * PlayerMover.speed);

			if(checkDistance)
			{
				//check if other object is bellow this one
				hit = Physics2D.Raycast (rb2D.position, -Vector2.up, distanceBetweenOtherObject);

				//if there is one, change it's speed to match this object's speed to not go go through each-other if they have different speeds.
				if(hit.collider != null && hit.collider.tag == "Car")
					hit.collider.GetComponent<Mover>().speed = this.speed;
			}
		}
		else
			rb2D.isKinematic = true;	//stop this object
	}
}

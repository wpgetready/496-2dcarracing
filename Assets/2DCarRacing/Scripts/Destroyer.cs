﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour 
{
	//this script is put on "Destroyer" object in hierarchy and is used to destroy object's when exited from trigger (they won't be visible in camera)
	void OnTriggerExit2D (Collider2D other)
	{
		if(other.tag == "Car" || other.tag ==  "Coin")
		{
			Destroy(other.gameObject);
		}
	}
}

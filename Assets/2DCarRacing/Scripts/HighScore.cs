﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScore : MonoBehaviour 
{
	public Text highScoreText;
	private float highScore;
	
	void Start ()
	{
		highScore = PlayerPrefs.GetFloat("InfiniteCarRacingHighScore");  //get saved high score
		highScoreText.text = "HighScore: " + highScore; //assign highScore value to highScoreText
	}

	void Update ()
	{
		if (PlayerMover.totalScore > highScore)
		{
			PlayerPrefs.SetFloat("InfiniteCarRacingHighScore", Mathf.Round(PlayerMover.totalScore));		//save totalScore
			highScoreText.text = "HighScore: " + Mathf.Round(PlayerMover.totalScore);	//assign totalScore value to highScoreText
		}
	}
}
